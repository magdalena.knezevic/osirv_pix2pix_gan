import tensorflow as tf
import os
import pathlib
import time
import datetime
from IPython import display
import cv2 
import matplotlib.pyplot as plt
import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import torchvision.transforms as transforms
import torchvision
from pathlib import Path
from functions import generate_images, load_org_noisy, gaussian, create_window, ssim, PSNR, generate_metrics, add_impulse_noise
from functions import load_image_train, load_image_test
from generator import downsample, upsample, Generator, gen_output, loss_object, generator_loss
from discriminator import Discriminator, discriminator_loss
from codeForPix2Pix import test_dataset
from ignite.engine import *
from torchmetrics.image import StructuralSimilarityIndexMeasure
import numpy as np
import torch.nn.functional as F

#MODEL ZA ČIŠĆENJE SLIKA OD SALT AND PEPPER ŠUMA

# Specify the path to the directory containing images
directory_path_train = './facades/train'
directory_path_test = './facades/test'
directory_path_val = './facades/val'
i=0

generator_sp = Generator()
discriminator_sp = Discriminator()

generator_sp_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
discriminator_sp_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

checkpoint_dir_sp = './training_checkpoints_sp'
checkpoint_prefix_sp = os.path.join(checkpoint_dir_sp, "ckpt")
checkpoint_sp = tf.train.Checkpoint(generator_optimizer=generator_sp_optimizer,
                                 discriminator_optimizer=discriminator_sp_optimizer,
                                 generator=generator_sp,
                                 discriminator=discriminator_sp)


# # Iterate over files in the directory
# for filename in os.listdir(directory_path_train):
#     i = i + 1
#     # Construct the full file path
#     file_path = os.path.join(directory_path_train, filename)
#     image = cv2.imread(file_path)
#     gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     gray_image = cv2.cvtColor(gray_image, cv2.COLOR_GRAY2RGB)
#     w = gray_image.shape[1]
#     w = w // 2
#     real_image = gray_image[:, :w]
#     sp_image = add_impulse_noise(real_image, 0.01, 0.01)

#     combined_image = np.hstack((real_image, sp_image))

#     #plt.imshow(combined_image)
#     #plt.show()
#     #print(combined_image.shape)
#     #plt.imsave('./salt_and_pepper/train' /f"img_{i}.jpg", combined_image)
#     output_path = os.path.join('./salt_and_pepper/train', f'img_{i}.jpg')
#     plt.imsave(output_path, combined_image)


# # Iterate over files in the directory
# for j in range(1, min(106, 106) + 1):
#     # Construct the full file path
#     file_path_test = os.path.join(directory_path_test, f'{j}.jpg')
#     image = cv2.imread(file_path_test)
#     gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     gray_image = cv2.cvtColor(gray_image, cv2.COLOR_GRAY2RGB)
#     w = gray_image.shape[1]
#     w = w // 2
#     real_image = gray_image[:, :w]
#     sp_image = add_impulse_noise(real_image, 0.01, 0.01)

#     combined_image_test = np.hstack((real_image, sp_image))

#     output_path_test = os.path.join('./salt_and_pepper/test', f'img_{j}.jpg')
#     plt.imsave(output_path_test, combined_image_test)



# for k in range(1, min(100, 100) + 1):
#     # Construct the full file path
#     file_path_val = os.path.join(directory_path_val, f'{k}.jpg')
#     image = cv2.imread(file_path_val)
#     gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     gray_image = cv2.cvtColor(gray_image, cv2.COLOR_GRAY2RGB)
#     w = gray_image.shape[1]
#     w = w // 2
#     real_image = gray_image[:, :w]
#     sp_image = add_impulse_noise(real_image, 0.01, 0.01)

#     combined_image_val = np.hstack((real_image, sp_image))

#     output_path_val = os.path.join('./salt_and_pepper/validation', f'img_{k}.jpg')
#     plt.imsave(output_path_val, combined_image_val)



BUFFER_SIZE = 400
BATCH_SIZE = 1
IMG_WIDTH = 256
IMG_HEIGHT = 256

train_path = './salt_and_pepper/train'
train_dataset_sp = tf.data.Dataset.list_files(os.path.join('./salt_and_pepper/train', 'img_*.jpg'))
train_dataset_sp = train_dataset_sp.map(load_image_train,
                                  num_parallel_calls=tf.data.AUTOTUNE)
train_dataset_sp = train_dataset_sp.shuffle(BUFFER_SIZE)
train_dataset_sp = train_dataset_sp.batch(BATCH_SIZE)

try:
    test_dataset_sp = tf.data.Dataset.list_files(os.path.join('./salt_and_pepper/test', 'img_*.jpg'))
except tf.errors.InvalidArgumentError:
    test_dataset_sp = tf.data.Dataset.list_files(os.path.join('./salt_and_pepper/validation', 'img_*.jpg'))
test_dataset_sp = test_dataset_sp.map(load_image_test)
test_dataset_sp = test_dataset_sp.batch(BATCH_SIZE)


log_dir="logs_sp/"

summary_writer = tf.summary.create_file_writer(
    log_dir + "fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))



@tf.function
def train_step(input_image, target, step):
  with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
    gen_output = generator_sp(input_image, training=True)

    disc_real_output = discriminator_sp([input_image, target], training=True)
    disc_generated_output = discriminator_sp([input_image, gen_output], training=True)

    gen_total_loss, gen_gan_loss, gen_l1_loss = generator_loss(disc_generated_output, gen_output, target)
    disc_loss = discriminator_loss(disc_real_output, disc_generated_output)
  


  generator_gradients = gen_tape.gradient(gen_total_loss,
                                          generator_sp.trainable_variables)
  discriminator_gradients = disc_tape.gradient(disc_loss,
                                               discriminator_sp.trainable_variables)

  generator_sp_optimizer.apply_gradients(zip(generator_gradients,
                                          generator_sp.trainable_variables))
  discriminator_sp_optimizer.apply_gradients(zip(discriminator_gradients,
                                              discriminator_sp.trainable_variables))


  with summary_writer.as_default():
    tf.summary.scalar('gen_total_loss', gen_total_loss, step=step//1000)
    tf.summary.scalar('gen_gan_loss', gen_gan_loss, step=step//1000)
    tf.summary.scalar('gen_l1_loss', gen_l1_loss, step=step//1000)
    tf.summary.scalar('disc_loss', disc_loss, step=step//1000)



def fit(train_ds, steps):
  start = time.time()

  psnr_training = []
  ssim_train_values = []

  for step, (input_image, target) in train_ds.repeat().take(steps).enumerate():
    if (step) % 1000 == 0:
      display.clear_output(wait=True)

      if step != 0:
        print(f'Time taken for 1000 steps: {time.time()-start:.2f} sec\n')

      start = time.time()

      psnr_train, ssim_score_train = generate_metrics(generator_sp, input_image, target)
      print(f"Step: {step//1000}k")

    train_step(input_image, target, steps)

    psnr_training.append(psnr_train)
    ssim_train_values.append(ssim_score_train)

    # Training step
    if (step+1) % 10 == 0:
      print(f"Step [{step + 1}/{steps}] ")


      with summary_writer.as_default():
        tf.summary.scalar('psnr_train', psnr_train, step=step//1000)
        tf.summary.scalar('ssim_train', ssim_score_train, step=step//1000)



    # Save (checkpoint) the model every 5k steps
    if (step + 1) % 1000 == 0:
      checkpoint_sp.save(file_prefix=checkpoint_prefix_sp)
      print(f'PSNR on training: {psnr_train}\n')
      print(f'SSIM on training: {ssim_score_train}\n')

#fit(train_dataset_sp, steps=60000)

 
checkpoint_sp.restore(tf.train.latest_checkpoint(checkpoint_dir_sp))

# # Run the trained model on a few examples from the test set
total_psnr_loop = 0
total_ssim_loop = 0
i = 0
for inp, tar in test_dataset_sp:
   psnr_test, ssim_score_test = generate_metrics(generator_sp, inp, tar)
   total_psnr_loop += psnr_test
   total_ssim_loop += ssim_score_test
   i = i +1

   with summary_writer.as_default():
      tf.summary.scalar('psnr_test', psnr_test, step=i//5)
      tf.summary.scalar('ssim_test', ssim_score_test, step=i//5)
   #print(f"PSNR: {psnr_test}")
   #print(f"Ssim: {ssim_score_test}")

total_psnr_loop = total_psnr_loop / i
total_ssim_loop = total_ssim_loop / i
print(f"Total PSNR on test set: {total_psnr_loop}")
print(f"Total SSIM on test set: {total_ssim_loop}")



###EVALUACIJA MODELA LOGS_SP NA COMBINED GAUSS DATASETU
total_psnr_loop = 0
total_ssim_loop = 0
i = 0
for inp, tar in test_dataset:
   psnr_test, ssim_score_test = generate_metrics(generator_sp, inp, tar)
   total_psnr_loop += psnr_test
   total_ssim_loop += ssim_score_test
   i = i +1


total_psnr_loop = total_psnr_loop / i
total_ssim_loop = total_ssim_loop / i
print(f"Total PSNR on test set: {total_psnr_loop}")  ##56.62
print(f"Total SSIM on test set: {total_ssim_loop}")  ##0.9
print(i)

for inp, tar in test_dataset_sp.take(1):
  generate_images(generator_sp, inp, tar)

















