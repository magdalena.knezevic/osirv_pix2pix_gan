import tensorflow as tf
import os
import pathlib
import time
import datetime
from IPython import display
import cv2 
import matplotlib.pyplot as plt
import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import torchvision.transforms as transforms
import torchvision
from pathlib import Path
from functions import generate_images, load_org_noisy, gaussian, create_window, ssim, PSNR, generate_metrics
from codeForPix2Pix import test_dataset, train_dataset, original, noisy
from generator import downsample, upsample, Generator, gen_output, loss_object, generator, generator_loss
from discriminator import Discriminator, discriminator_loss, discriminator_optimizer, generator_optimizer, checkpoint_dir, checkpoint
from discriminator import discriminator, checkpoint_prefix
from torchmetrics.image import PeakSignalNoiseRatio
from ignite.engine import *
import numpy as np
import torch.nn.functional as F

log_dir="model_three/"

summary_writer_three = tf.summary.create_file_writer(
  log_dir + "fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))



checkpoint_dir_three = './training_checkpoints_three'
checkpoint_prefix_three = os.path.join(checkpoint_dir_three, "ckpt")
checkpoint_three = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                 discriminator_optimizer=discriminator_optimizer,
                                 generator=generator,
                                 discriminator=discriminator)

data_range = 255.0
gauss_dis = gaussian(11, 1.5)
window = create_window(11, 3)
tensorify = lambda x: torch.Tensor(x.transpose((2, 0, 1))).unsqueeze(0).float().div(255.0)


@tf.function
def train_step(input_image, target, step):
  with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
    gen_output = generator(input_image, training=True)

    disc_real_output = discriminator([input_image, target], training=True)
    disc_generated_output = discriminator([input_image, gen_output], training=True)

    gen_total_loss, gen_gan_loss, gen_l1_loss = generator_loss(disc_generated_output, gen_output, target)
    disc_loss = discriminator_loss(disc_real_output, disc_generated_output)
  


  generator_gradients = gen_tape.gradient(gen_total_loss,
                                          generator.trainable_variables)
  discriminator_gradients = disc_tape.gradient(disc_loss,
                                               discriminator.trainable_variables)

  generator_optimizer.apply_gradients(zip(generator_gradients,
                                          generator.trainable_variables))
  discriminator_optimizer.apply_gradients(zip(discriminator_gradients,
                                              discriminator.trainable_variables))


  with summary_writer_three.as_default():
    tf.summary.scalar('gen_total_loss', gen_total_loss, step=step//1000)
    tf.summary.scalar('gen_gan_loss', gen_gan_loss, step=step//1000)
    tf.summary.scalar('gen_l1_loss', gen_l1_loss, step=step//1000)
    tf.summary.scalar('disc_loss', disc_loss, step=step//1000)

  

def fit(train_ds, steps):
  start = time.time()

  psnr_training = []
  ssim_train_values = []
  


  for step, (input_image, target) in train_ds.repeat().take(steps).enumerate():
    if (step) % 1000 == 0:
      display.clear_output(wait=True)

      if step != 0:
        print(f'Time taken for 1000 steps: {time.time()-start:.2f} sec\n')

      start = time.time()

      psnr_train, ssim_score_train = generate_metrics(generator, input_image, target)
      print(f"Step: {step//1000}k")

    train_step(input_image, target, step)


    psnr_training.append(psnr_train)
    ssim_train_values.append(ssim_score_train)

    # Training step
    if (step+1) % 10 == 0:
      print(f"Step [{step + 1}/{steps}] ")


      with summary_writer_three.as_default():
        tf.summary.scalar('psnr_train', psnr_train, step=step//1000)
        tf.summary.scalar('ssim_train', ssim_score_train, step=step//1000)
      


    # Save (checkpoint) the model every 5k steps
    if (step + 1) % 1000 == 0:
      checkpoint_three.save(file_prefix=checkpoint_prefix_three)
      print(f'PSNR on training: {psnr_train}\n')
      print(f'SSIM on training: {ssim_score_train}\n')





#fit(train_dataset, steps=60000)

      
checkpoint.restore(tf.train.latest_checkpoint('./training_checkpoints_three'))
# checkpoint_path = './training_checkpoints/ckpt-15.index' 
# checkpoint.restore(checkpoint_path)

#Run the trained model on a few examples from the test set
total_psnr_loop = 0
total_ssim_loop = 0
i = 0
for inp, tar in test_dataset:
   psnr_test, ssim_score_test = generate_metrics(generator, inp, tar)
   total_psnr_loop += psnr_test
   total_ssim_loop += ssim_score_test
   i = i + 1

total_psnr_loop = total_psnr_loop / i
total_ssim_loop = total_ssim_loop / i
print(f"Total PSNR on test set: {total_psnr_loop}")
print(f"Total SSIM on test set: {total_ssim_loop}")

for inp, tar in test_dataset.take(1):
  generate_images(generator, inp, tar)











