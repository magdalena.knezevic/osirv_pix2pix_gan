import tensorflow as tf
import os
import pathlib
import time
import datetime
from matplotlib import pyplot as plt
from IPython import display
import cv2 as cv2
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
from numpy import asarray
from numpy import vstack
import numpy as np
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from numpy import savez_compressed
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import torchvision.transforms as transforms
import torchvision
from pathlib import Path
from functions import load, load_org_noisy, gaussian_noise, sp_noise, add_uniform_noise, random_crop, random_jitter, resize, normalize, load_image_train, load_image_test

#učitavanje dataseta
dataset_name = "facades"

_URL = f'http://efrosgans.eecs.berkeley.edu/pix2pix/datasets/{dataset_name}.tar.gz'

path_to_zip = tf.keras.utils.get_file(
    fname=f"{dataset_name}.tar.gz",
    origin=_URL,
    extract=True)

path_to_zip  = pathlib.Path(path_to_zip)

PATH = path_to_zip.parent/dataset_name
list(PATH.parent.iterdir())

MY_PATH = Path('./combined/gauss')


# #dodavanje šuma na slike za treniranje, GAUSS
# mu=0
# sigma=1 
# for i in range(1, 401):
#     image_path = PATH / f"train/{i}.jpg"
#     real_image = load(str(image_path))
#     real_image = real_image / 255.0
#     #print("Real image:", real_image.shape)

#     # Add Gaussian noise to the loaded image
#     noisy_image = real_image + gaussian_noise(real_image.numpy(), mu, sigma)
#     #print("Noisy image:", noisy_image.shape)

#     # Save the noisy image or use it as needed
#     path_gauss = Path("./gauss_noisy_images/train")
#     gauss_noisy_image_path = path_gauss / f"noisy_{i}.jpg"
#     plt.imsave(str(gauss_noisy_image_path), np.clip(noisy_image.numpy(), 0, 1))
# # re = plt.imread("./gauss_noisy_images/train/noisy_1.jpg")
# # print("Učitana slika:", re.shape)
# # plt.imshow(re)
# # plt.show()

#dodavanje šuma na slike za testiranje, GAUSS
# mu=0
# sigma=1 #chat kaze 25
# for i in range(1, 107):
#     image_path_test = PATH / f"test/{i}.jpg"
#     real_image_test = load(str(image_path_test))
#     real_image_test = real_image_test / 255.0
#     #print("Real image:", real_image.shape)

#     # Add Gaussian noise to the loaded image
#     noisy_image_test = real_image_test + gaussian_noise(real_image_test.numpy(), mu, sigma)
#     #print("Noisy image:", noisy_image.shape)

#     # Save the noisy image or use it as needed
#     path_gauss_test = Path("./gauss_noisy_images/test")
#     gauss_noisy_image_path_test = path_gauss_test / f"noisy_{i}.jpg"
#     plt.imsave(str(gauss_noisy_image_path_test), np.clip(noisy_image_test.numpy(), 0, 1))
# re = plt.imread("./gauss_noisy_images/test/noisy_1.jpg")
# print("Učitana slika:", re.shape)
# plt.imshow(re)
# plt.show()

#dodavanje šuma na slike za validaciju, GAUSS
# mu=0
# sigma=1 #chat kaze 25
# for i in range(1, 101):
#     image_path_val = PATH / f"val/{i}.jpg"
#     real_image_val = load(str(image_path_val))
#     real_image_val = real_image_val / 255.0
#     #print("Real image:", real_image.shape)

#     # Add Gaussian noise to the loaded image
#     noisy_image_val = real_image_val + gaussian_noise(real_image_val.numpy(), mu, sigma)
#     #print("Noisy image:", noisy_image.shape)

#     # Save the noisy image or use it as needed
#     path_gauss_val = Path("./gauss_noisy_images/val")
#     gauss_noisy_image_path_val = path_gauss_val / f"noisy_{i}.jpg"
#     plt.imsave(str(gauss_noisy_image_path_val), np.clip(noisy_image_val.numpy(), 0, 1))
# re = plt.imread("./gauss_noisy_images/test/noisy_1.jpg")
# print("Učitana slika:", re.shape)
# plt.imshow(re)
# plt.show()
# re.resize([1, 256, 256, 3])
# print(re.shape)
# re.resize([256, 256, 3])
# print(re.shape)

# #dodavanje šuma na slike za treniranje, SALT_PEPPER
# prob = 0.05
# for i in range(1, 401):
#     image_path = PATH / f"train/{i}.jpg"
#     org_image = load(str(image_path))
#     org_image = org_image / 255.0
#     #print("Real image:", real_image.shape)

#     # Add Gaussian noise to the loaded image
#     salt_pepper_image = org_image + sp_noise(org_image.numpy(), prob)
#     #print("Noisy image:", noisy_image.shape)

#     # Save the noisy image or use it as needed
#     path_sp = Path("./salt_n_pepper_images/train")
#     salt_n_pepper_image_path = path_sp / f"noisy_{i}.jpg"
#     plt.imsave(str(salt_n_pepper_image_path), np.clip(salt_pepper_image.numpy(), 0, 1))



#dodavanje šuma na slike za testiranje, SALT_PEPPER
# prob = 0.05
# for i in range(1, 107):
#     image_path_test = PATH / f"test/{i}.jpg"
#     org_image_test = load(str(image_path_test))
#     org_image_test = org_image_test / 255.0
#     #print("Real image:", real_image.shape)

#     # Add Gaussian noise to the loaded image
#     salt_pepper_image_test = org_image_test + sp_noise(org_image_test.numpy(), prob)
#     #print("Noisy image:", noisy_image.shape)
#     plt.imshow(salt_pepper_image_test)
#     plt.show()

#     # Save the noisy image or use it as needed
#     path_sp_test = Path("./salt_n_pepper_images/test")
#     salt_n_pepper_image_path_test = path_sp_test / f"noisy_{i}.jpg"
    #plt.imsave(str(salt_n_pepper_image_path_test), np.clip(salt_pepper_image_test.numpy(), 0, 1))

#dodavanje šuma na slike za validaciju, SALT_PEPPER
# prob = 0.05
# for i in range(1, 101):
#     image_path_val = PATH / f"val/{i}.jpg"
#     org_image_val = load(str(image_path_val))
#     org_image_val = org_image_val / 255.0
#     #print("Real image:", real_image.shape)

#     # Add Gaussian noise to the loaded image
#     salt_pepper_image_val = org_image_val + sp_noise(org_image_val.numpy(), prob)
#     #print("Noisy image:", noisy_image.shape)

#     # Save the noisy image or use it as needed
#     path_sp_val = Path("./salt_n_pepper_images/val")
#     salt_n_pepper_image_path_val = path_sp_val / f"noisy_{i}.jpg"
#     plt.imsave(str(salt_n_pepper_image_path_val), np.clip(salt_pepper_image_val.numpy(), 0, 1))


##DODAVANJE ŠUMA NA SLIKE ZA TRENIRANJE, UNIFORMNI ŠUM
# low=0.5
# high=2.0
# strength=0.2
# for i in range(1, 401):
    # image_path = PATH / f"train/{i}.jpg"
    # original_image = load(str(image_path))
    # original_image = original_image / 255.0
    # #print("Real image:", original_image.shape)

    # # Add Gaussian noise to the loaded image
    # uniform_image = original_image + add_uniform_noise(original_image.numpy(), low, high, strength)
    # #print("Noisy image:", uniform_image.shape)

    # # Save the noisy image or use it as needed
    # path_uniform = Path("./uniform_images/train")
    # uniform_image_path = path_uniform / f"noisy_{i}.jpg"
    # plt.imsave(str(uniform_image_path), np.clip(uniform_image.numpy(), 0, 1))

#DODAVANJE ŠUMA NA SLIKE ZA TESTIRANJE, UNIFORMNI ŠUM
# low=0.5
# high=2.0
# strength=0.2
# for i in range(1, 107):
    # image_path_test = PATH / f"test/{i}.jpg"
    # original_image_test = load(str(image_path_test))
    # original_image_test = original_image_test / 255.0
    # #print("Real image:", original_image.shape)

    # # Add Gaussian noise to the loaded image
    # uniform_image_test = original_image_test + add_uniform_noise(original_image_test.numpy(), low, high, strength)
    # #print("Noisy image:", uniform_image.shape)

    # # Save the noisy image or use it as needed
    # path_uniform_test = Path("./uniform_images/test")
    # uniform_image_path_test = path_uniform_test / f"noisy_{i}.jpg"
    # plt.imsave(str(uniform_image_path_test), np.clip(uniform_image_test.numpy(), 0, 1))


##DODAVANJE ŠUMA NA SLIKE ZA VALIDACIJU, UNIFORMNI ŠUM
# low=0.5
# high=2.0
# strength=0.2
# for i in range(1, 101):
#     image_path_val = PATH / f"val/{i}.jpg"
#     original_image_val = load(str(image_path_val))
#     original_image_val = original_image_val / 255.0
#     #print("Real image:", original_image.shape)

#     # Add Gaussian noise to the loaded image
#     uniform_image_val = original_image_val + add_uniform_noise(original_image_val.numpy(), low, high, strength)
#     #print("Noisy image:", uniform_image.shape)

#     # Save the noisy image or use it as needed
#     path_uniform_val = Path("./uniform_images/val")
#     uniform_image_path_val = path_uniform_val / f"noisy_{i}.jpg"
#     plt.imsave(str(uniform_image_path_val), np.clip(uniform_image_val.numpy(), 0, 1))



#JITTERING sa fjama iz functions.py
original = load(str(PATH / 'train/1.jpg'))
# Casting to int for matplotlib to display the images
original = original / 255.0
noisy, _ = load_org_noisy(str(MY_PATH / 'train/img_1.jpg'))
noisy = noisy / 255.0
# plt.imshow(noisy)
# plt.show()



##KOMBINIRANJE SLIKA
#GAUSS, TRAIN
# for i in range(1, min(400, 400) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'train/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./gauss_noisy_images/train/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/train")
#     path_combined_gauss_train = path_combined / f"img_{i}.jpg"
#     plt.imsave(str(path_combined_gauss_train), combined_image)

#GAUSS, TEST
# for i in range(1, min(106, 106) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'test/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./gauss_noisy_images/test/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/test")
#     path_combined_gauss_train = path_combined / f"img_{i}.jpg"
#     plt.imsave(str(path_combined_gauss_train), combined_image)

#GAUSS, VAL
# for i in range(1, min(100, 100) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'val/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./gauss_noisy_images/val/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/val")
#     path_combined_gauss_train = path_combined / f"img_{i}.jpg"
#     plt.imsave(str(path_combined_gauss_train), combined_image)



##SP, TRAIN 
# for i in range(1, min(400, 400) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'train/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./salt_n_pepper_images/train/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/train")
#     path_combined_sp_train = path_combined / f"img_{i+400}.jpg"
#     plt.imsave(str(path_combined_sp_train), combined_image)


##SP, TEST 
# for i in range(1, min(106, 106) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'test/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./salt_n_pepper_images/test/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/test")
#     path_combined_sp_train = path_combined / f"img_{i+106}.jpg"
#     plt.imsave(str(path_combined_sp_train), combined_image)

##SP, VAL
# for i in range(1, min(100, 100) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'val/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./salt_n_pepper_images/val/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/val")
#     path_combined_sp_train = path_combined / f"img_{i+100}.jpg"
#     plt.imsave(str(path_combined_sp_train), combined_image)


#UNIFORM, TRAIN
# for i in range(1, min(400, 400) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'train/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./uniform_images/train/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/train")
#     path_combined_uniform_train = path_combined / f"img_{i+800}.jpg"
#     plt.imsave(str(path_combined_uniform_train), combined_image)


#UNIFORM, TEST
# for i in range(1, min(106, 106) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'test/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./uniform_images/test/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/test")
#     path_combined_uniform_test = path_combined / f"img_{i+212}.jpg"
#     plt.imsave(str(path_combined_uniform_test), combined_image)

#UNIFORM, VAL
# for i in range(1, min(100, 100) + 1):
#     # Load images from the first dataset
#     original = load(str(PATH / f'val/{i}.jpg'))
#     original = original / 255.0  # Casting to int for matplotlib to display the images

#     # Load images from the second dataset
#     noisy_path = f"./uniform_images/val/noisy_{i}.jpg"
#     noisy = plt.imread(noisy_path)
#     noisy = noisy / 255.0

#     # Combine images
#     combined_image = np.hstack((original, noisy))

#     # Display or save the combined image as needed
#     # plt.imshow(combined_image)
#     # plt.show()

#     # If you want to save the combined image, you can use plt.imsave or another method
#     path_combined = Path("./combined/gauss/val")
#     path_combined_uniform_val = path_combined / f"img_{i+200}.jpg"
#     plt.imsave(str(path_combined_uniform_val), combined_image)

# print(original.shape)
# print(noisy.shape)

# The facade training set consist of 400 images
BUFFER_SIZE = 1200
# The batch size of 1 produced better results for the U-Net in the original pix2pix experiment
BATCH_SIZE = 1
# Each image is 256x256 in size
IMG_WIDTH = 256
IMG_HEIGHT = 256


#BUILDING AN INPUT PIPELINE
train_dataset = tf.data.Dataset.list_files(str(MY_PATH / 'train/img_*.jpg'))
train_dataset = train_dataset.map(load_image_train,
                                   num_parallel_calls=tf.data.AUTOTUNE)
train_dataset = train_dataset.shuffle(BUFFER_SIZE)
train_dataset = train_dataset.batch(BATCH_SIZE)

try:
    test_dataset = tf.data.Dataset.list_files(str(MY_PATH / 'test/img_*.jpg'))
except tf.errors.InvalidArgumentError:
    test_dataset = tf.data.Dataset.list_files(str(MY_PATH / 'val/img_*.jpg'))
test_dataset = test_dataset.map(load_image_test)
test_dataset = test_dataset.batch(BATCH_SIZE)


  



    



























