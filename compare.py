import tensorflow as tf
from matplotlib import pyplot as plt
from IPython import display
import cv2 as cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.preprocessing.image import load_img
import random
import torch
from pathlib import Path
from functions import PSNR, load
from torchmetrics.image import PeakSignalNoiseRatio
from ignite.engine import *

#DOKUMENT ZA VJEZBU, PROVJERAVANJE, NIJE U SKLOPU KODA ZA STVARANJE MODELA
#učitavanje dataseta
dataset_name = "facades"

_URL = f'http://efrosgans.eecs.berkeley.edu/pix2pix/datasets/{dataset_name}.tar.gz'

path_to_zip = tf.keras.utils.get_file(
    fname=f"{dataset_name}.tar.gz",
    origin=_URL,
    extract=True)

# path_to_zip  = pathlib.Path(path_to_zip)

# PATH = path_to_zip.parent/dataset_name
# list(PATH.parent.iterdir())

# image_path = PATH / f"train/1.jpg"
# real_image = load(str(image_path))

# print(real_image.dtype)

# noisy = cv2.imread('./gauss_noisy_images/train/noisy_1.jpg')

# print(noisy.dtype)

# tensor_real = tf.convert_to_tensor(real_image, dtype=tf.float32)
# tensor_noisy = tf.convert_to_tensor(noisy, dtype=tf.float32)

# print(tensor_real.dtype)
# print(tensor_noisy.dtype)

# psnr = PSNR(tensor_real, tensor_noisy)
# print(f"PSNR: {psnr}")

target = cv2.imread('./predictions/target.jpg')
predicted = cv2.imread('./predictions/predicted.jpg')
target = cv2.cvtColor(target, cv2.COLOR_BGR2RGB)
predicted = cv2.cvtColor(predicted, cv2.COLOR_BGR2RGB)

# plt.imshow(target)
# plt.show()
# print(target.shape, target.dtype)
# plt.imshow(predicted)
# plt.show()
# print(predicted.shape, predicted.dtype)
# psnr_target = PSNR(target, predicted)
# print(psnr_target)

combined = cv2.imread('./combined/gauss/train/img_1.jpg')
lena = cv2.imread('./lena.jpg')
real = load('./combined/gauss/train/img_1.jpg')
plt.imshow(real/255.0)
plt.show()
print(real.dtype)

def add_impulse_noise(image, salt_prob, pepper_prob):
    noisy_image = np.copy(image)

    # Add salt noise
    salt_mask = np.random.rand(*image.shape[:2]) < salt_prob
    noisy_image[salt_mask] = 255

    # Add pepper noise
    pepper_mask = np.random.rand(*image.shape[:2]) < pepper_prob
    noisy_image[pepper_mask] = 0

    return noisy_image

def sp_noise(image,prob):
    output = np.zeros(image.shape,np.uint8)
    thres = 1 - prob 
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()
            if rdn > thres:
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    return output

noisy_real = real + sp_noise(real, 0.05)
plt.imshow(noisy_real)
plt.title('Noisy Real Image')
plt.show()


target = target / 255.0

target_noisy = sp_noise(target, 0.05)


lena_noisy = add_impulse_noise(lena, 0.05, 0.05)
plt.imshow(lena_noisy)
plt.show()

psnr_lena = PSNR(lena, lena_noisy)
print(psnr_lena)

#target = tf.convert_to_tensor(target, dtype=tf.float32)

plt.imshow(target)
plt.show()
print(target.dtype)
plt.imshow(target_noisy)
plt.show()
plt.imshow(predicted)
plt.show()
plt.imshow(combined)
plt.show()

#target = tf.convert_to_tensor(target, dtype=tf.float32)
#predicted = tf.convert_to_tensor(predicted, dtype=tf.float32)

target = cv2.cvtColor(target, cv2.COLOR_BGR2GRAY)
predicted = cv2.cvtColor(predicted, cv2.COLOR_BGR2GRAY)

pred = torch.tensor(predicted)
target = torch.tensor(target)

pred = torch.reshape(pred, (256, 256))
target = torch.reshape(target, (256, 256))

print(target.dtype)
print(pred.dtype)

#psnr = PSNR(target, predicted)
#psnr = PeakSignalNoiseRatio(target, target)


print(target.shape)
print(pred.shape)



# psnr_test = PeakSignalNoiseRatio()
# psnr_test.update = psnr_test((target), (pred))
# result = psnr_test.compute()
# print(f"PSNR: {result}")
result = PSNR(target.numpy(), pred.numpy())
print(f"PSNR: {result}")
plt.imshow(target)
plt.show()
plt.imshow(pred)
plt.show()




#lena = cv2.imread("./lena.jpg")
# lena = cv2.imread("./")
# lena = cv2.resize(lena, (256,256))
# cv2.imwrite("lena.jpg", lena)
# plt.imshow(lena)
# plt.show()
#print(lena.shape)

#photographer = cv2.imread("./photographer.jpg")
# plt.imshow(photographer)
# plt.show()
# print(photographer.shape)

# Convert the NumPy array to a TensorFlow tensor
# tensor_lena = tf.convert_to_tensor(lena, dtype=tf.float32)
# tensor_photo = tf.convert_to_tensor(photographer, dtype=tf.float32)

# # Now you can use tensor_image.numpy() if you want to convert it back to a NumPy array
# #numpy_lena = tensor_lena.numpy()
# #numpy_photo = tensor_photo.numpy()

# print(tensor_lena.shape)
# print(tensor_photo.shape)

# psnr = PSNR(tensor_lena, tensor_photo)
# print(f"PSNR: {psnr}")